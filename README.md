[![Build Status](http://img.shields.io/travis/budjb/grails-rabbitmq-native.svg?branch=grails-2.x)](https://travis-ci.org/budjb/grails-rabbitmq-native)
[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

## Why customize this build ? ##

The reason why we needed to customize this build was because the default TLS being used as TLSv1 which is very old and weak and should not be used in a production environment. So we make a single line change to force usage of TLSv1.2 in the ConnectionContextImpl.groovy class.

The original code can be found here: https://github.com/budjb/grails-rabbitmq-native/tree/grails-2.x

## Building ##

Please use grails-2.5.4 and jdk1.8

```
C:\grails\grails-2.5.4\bin\grails -Dhttps.protocols=TLSv1.2 refresh-dependencies
C:\grails\grails-2.5.4\bin\grails -Dhttps.protocols=TLSv1.2 maven-install
```

RabbitMQ Native plugin for Grails
---------------------------------
See the documentation at http://budjb.github.io/grails-rabbitmq-native/2.x/latest/.
